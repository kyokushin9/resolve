<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Shelude;

class EmployeeScheduleController extends Controller
{
    public function getWorkSchedule(Request $request): JsonResponse
    {

            try
            {
                $shelude = new Shelude();
                if(!empty($request->endpoint)) {
                    $response = $shelude->listSheludeEndpoint($request->startDate,$request->endDate);
                } else {
                    $response = $shelude->listShelude($request->startDate,$request->endDate);
                }
                
        
            }
            catch (Exception $e)
            {

                return new JsonResponse(['error'], JsonResponse::HTTP_NOT_IMPLEMENTED);
            }

            return new JsonResponse($response, JsonResponse::HTTP_NOT_IMPLEMENTED);

        //return new JsonResponse(['Endpoint not yet implemented.'], JsonResponse::HTTP_NOT_IMPLEMENTED);
    }
}
