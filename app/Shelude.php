<?php

namespace App;

use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shelude extends Model
{
    use HasFactory;

    public function listShelude($startDay, $endDay) {
        
        $arHolidays = $this->arHolidays();
        
        $start    = new DateTime($startDay);
        $end      = new DateTime($endDay);
        
        $interval = DateInterval::createFromDateString('1 day');
        $period   = new DatePeriod($start, $interval, $end);

        $businessDays = 0;
        $arrDay = array();
        $i = 0;
        
        foreach ($period as $day) {

            if (! in_array($day->format('w'), [0, 6]) && !in_array($day->format('d.m.Y'), $arHolidays)) {
                $businessDays++;
                $arrDay['schedule'][$i]['day'] = $day->format('Y-m-d');
                $arrDay['schedule'][$i]['timeRanges'][0] = array('start'=>'10:00');
                $arrDay['schedule'][$i]['timeRanges'][0] = array('end'=>'13:00');
                $arrDay['schedule'][$i]['timeRanges'][1] = array('start'=>'14:00');
                $arrDay['schedule'][$i]['timeRanges'][1] = array('end'=>'19:00');
                $i++;
           }
           
        }

        return json_encode($arrDay);
    }

    public function listSheludeEndpoint($startDay, $endDay) {

        $arHolidays = $this->arHolidays();
        
        $start    = new DateTime($startDay);
        $end      = new DateTime($endDay);
        
        $interval = DateInterval::createFromDateString('1 day');
        $period   = new DatePeriod($start, $interval, $end);

        $businessDays = 0;
        $arrDay = array();
        $i = 0;
        
        foreach ($period as $day) {

            if ( in_array($day->format('w'), [0, 6]) || in_array($day->format('d.m.Y'), $arHolidays)) {
                $businessDays++;
                $arrDay['schedule'][$i]['day'] = $day->format('Y-m-d');
                $i++;
           }
           
        }

        return json_encode($arrDay);

    }

    public function arHolidays(){
        $calendar = simplexml_load_file('http://xmlcalendar.ru/data/ru/'.date('Y').'/calendar.xml');
 
        $calendar = $calendar->days->day;
        
        foreach( $calendar as $day ){
            $d = (array)$day->attributes()->d;
            $d = $d[0];
            $d = substr($d, 3, 2).'.'.substr($d, 0, 2).'.'.date('Y');

            if( $day->attributes()->t == 1 ) $arHolidays[] = $d;
        }
        
        return $arHolidays;
    }
}
